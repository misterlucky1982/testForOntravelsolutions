package by.votes.entityes;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="subjects")
public class Subject {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name = "id")
	private long id;
	@Column (name = "status", nullable = false)
	private boolean status;
	@ManyToOne(cascade= {CascadeType.REFRESH}, fetch=FetchType.LAZY)
	@JoinColumn(name="creator")
	private User creator;
	@Column (name = "question", nullable = false)
	private String mainQuestion;
	@OneToMany(mappedBy="subject", targetEntity=by.votes.entityes.Item.class)
	private Set<Item> items = new HashSet<>();
	
	public Subject(){
		
	}
	
	public Subject(User creator, String mainQuestion){
		this.creator = creator;
		this.mainQuestion = mainQuestion;
		this.status = true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public String getMainQuestion() {
		return mainQuestion;
	}

	public void setMainQuestion(String mainQuestion) {
		this.mainQuestion = mainQuestion;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}


	@Override
	public String toString() {
		return "Subject [id=" + id + ", status=" + status + ", creator=" + creator + ", mainQuestion=" + mainQuestion
				+ ", items=" + items + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subject other = (Subject) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public boolean isUserCanVote(User user){
		/*
		 * more correct way is defining it by constructing hql-request
		 * and I have to write it after if I have to go on with this project,
		 * but now I have no time to search examples in web and leave it in 
		 * that state for the time... 
		 */
		if(user==null)return false;
		Iterator it = this.items.iterator();
		while(it.hasNext()){
			Set<Vote>votes = ((Item)it.next()).getVotes();
			for(Vote v:votes){
				System.out.println(user.getId()==v.getUser().getId());
				System.out.println(user.getId()+"  =  "+v.getUser().getId());
				if(user.getId()==v.getUser().getId())return false;
			}
		}
		return true;
	}
	
}
