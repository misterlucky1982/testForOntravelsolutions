package by.votes.entityes;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="items")
public class Item {
	
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name = "id")
	private long id;
	@JoinColumn(name="subject")
	@ManyToOne(cascade= {CascadeType.REFRESH}, fetch=FetchType.LAZY)
	private Subject subject;
	@Column(name="text")
	private String itemText;
	@OneToMany(mappedBy="item",targetEntity=by.votes.entityes.Vote.class)
	private Set<Vote>votes = new HashSet<>();
	
	public Item(){
		
	}
	
	public Item(Subject subject, String text){
		this.subject = subject;
		this.itemText = text;
	}
	
	@Override
	public String toString() {
		return "Item [id=" + id + ", subject=" + subject + ", itemText=" + itemText + ", votes=" + votes + "]";
	}
	
	
	public String getItemText() {
		return itemText;
	}

	public void setItemText(String itemText) {
		this.itemText = itemText;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Set<Vote> getVotes() {
		return votes;
	}

	public void setVotes(Set<Vote> votes) {
		this.votes = votes;
	}
	
	

}
