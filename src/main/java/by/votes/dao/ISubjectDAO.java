package by.votes.dao;

import java.sql.SQLException;
import java.util.List;

import by.votes.entityes.Subject;
import by.votes.entityes.User;

public interface ISubjectDAO {
	
	Subject getSubjectById(long id) throws SQLException;
	List<Subject>getEntireSubjectsList() throws SQLException;
	List<Subject>getOpenSubjectsList() throws SQLException;
	List<Subject>getClosedSubjectsList() throws SQLException;
	List<Subject>getSubjectsByUser(User user) throws SQLException;
	boolean addSubject(Subject subject) throws SQLException;
	boolean ubdateSubject(Subject subject) throws SQLException;
	boolean deleteSubject(Subject subject) throws SQLException;
}
