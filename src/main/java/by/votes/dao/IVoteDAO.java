package by.votes.dao;

import java.sql.SQLException;
import java.util.List;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;
import by.votes.entityes.Vote;

public interface IVoteDAO {
	Vote getVoteById(long id) throws SQLException;
	List<Vote>getVotesByItem(Item item)throws SQLException;
	boolean addVote(Vote vote) throws SQLException;
	boolean updateVote(Vote vote) throws SQLException;
	boolean deleteVote(Vote vote) throws SQLException;
	List<Vote>getVotesBySubject(Subject subject) throws SQLException;
}
