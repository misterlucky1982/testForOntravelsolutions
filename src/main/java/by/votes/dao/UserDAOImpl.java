package by.votes.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import by.votes.entityes.User;
import by.votes.utils.DataBaseConnectionUtil;

@Transactional
@Repository
public class UserDAOImpl implements IUserDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public User getById(long id) throws SQLException {
		Session session = null;
		User user = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			user = (User) session.load("by.votes.entityes.User", id);
		} catch (Exception e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return user;
	}

	@Override
	public List<User> getUsersEntireList() throws SQLException {
		Session session = null;
		List<User> users = new ArrayList<>();
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			users = session.createQuery("from User").list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return users;
	}

	@Override
	public boolean addUser(User user) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean updateUser(User user) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(user);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean deleteUser(User user) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(user);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public User getUserByLogin(String login) throws SQLException {
		Session session = null;
		List<User> users = new ArrayList<User>();
		try {
			session = DataBaseConnectionUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			users = session.createQuery("from User where login='" + login + "'").list();
			session.getTransaction().commit();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		if (users.size() != 1)
			throw new SQLException();
		return users.get(0);
	}

}
