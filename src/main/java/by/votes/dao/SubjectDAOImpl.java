package by.votes.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import by.votes.entityes.Subject;
import by.votes.entityes.User;
import by.votes.utils.DataBaseConnectionUtil;

@Transactional
@Repository
public class SubjectDAOImpl implements ISubjectDAO{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Subject getSubjectById(long id) throws SQLException {
		Session session = null;
		Subject subject=null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			subject = (Subject) session.load("by.votes.entityes.Subject", id);
		} catch (Exception e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return subject;
	}

	@Override
	public List<Subject> getEntireSubjectsList() throws SQLException {
		Session session = null;
		List<Subject> subjects = new ArrayList<>();
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			subjects = session.createQuery("from Subject").list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return subjects;
	}

	@Override
	public List<Subject> getOpenSubjectsList() throws SQLException {
		Session session = null;
		List<Subject> subjects = new ArrayList<>();
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			subjects = session.createQuery("from Subject where status=true").list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return subjects;
	}

	@Override
	public List<Subject> getClosedSubjectsList() throws SQLException {
		Session session = null;
		List<Subject> subjects = new ArrayList<>();
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			subjects = session.createQuery("from Subject where status=false").list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return subjects;
	}

	@Override
	public List<Subject> getSubjectsByUser(User user) throws SQLException {
		Session session = null;
		List<Subject> subjects = new ArrayList<>();
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			subjects = session.createQuery("from Subject where user="+user.getId()).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return subjects;
	}

	@Override
	public boolean addSubject(Subject subject) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(subject);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean ubdateSubject(Subject subject) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(subject);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean deleteSubject(Subject subject) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(subject);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}


}
