package by.votes.dao;

import java.sql.SQLException;
import java.util.List;

import by.votes.entityes.Subject;
import by.votes.entityes.User;

public interface IUserDAO {
	
	User getById(long id) throws SQLException;
	User getUserByLogin(String login)throws SQLException;
	List<User>getUsersEntireList() throws SQLException;
	boolean addUser(User user) throws SQLException;
	boolean updateUser(User user) throws SQLException;
	boolean deleteUser(User user) throws SQLException;	
}
