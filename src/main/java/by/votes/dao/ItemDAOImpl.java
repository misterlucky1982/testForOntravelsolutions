package by.votes.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;
import by.votes.utils.DataBaseConnectionUtil;

@Transactional
@Repository
public class ItemDAOImpl implements IItemDAO{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Item getItemById(long id) throws SQLException {
		Session session = null;
		Item item=null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			item = (Item) session.load("by.votes.entityes.Item", id);
		} catch (Exception e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return item;
	}

	@Override
	public boolean addItem(Item item) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(item);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean updateItem(Item item) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(item);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean deleteItem(Item item) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(item);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public List<Item> getItemsBySubject(Subject subject) throws SQLException {
		Session session = null;
		List<Item> items = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			items = session.createQuery("from Item where subject="+subject.getId()).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		if(items!=null){
			return items;
		}else return new ArrayList<Item>();
	}

}
