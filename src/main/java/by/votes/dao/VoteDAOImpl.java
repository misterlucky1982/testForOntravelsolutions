package by.votes.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;
import by.votes.entityes.Vote;
import by.votes.utils.DataBaseConnectionUtil;

@Transactional
@Repository
public class VoteDAOImpl implements IVoteDAO{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Vote getVoteById(long id) throws SQLException {
		Session session = null;
		Vote vote=null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			vote = (Vote) session.load("by.votes.entityes.Vote", id);
		} catch (Exception e) {
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return vote;
	}

	@Override
	public boolean addVote(Vote vote) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(vote);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean updateVote(Vote vote) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(vote);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public boolean deleteVote(Vote vote) throws SQLException {
		Session session = null;
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.delete(vote);
			session.getTransaction().commit();
		} catch (Exception e) {
			return false;
		} finally {
			if (session != null && session.isOpen())
				session.close();
		}
		return true;
	}

	@Override
	public List<Vote> getVotesByItem(Item item) throws SQLException {
		Session session = null;
		List<Vote> votes = new ArrayList<>();
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			votes = session.createQuery("from Vote where item="+item.getId()).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return votes;
	}

	@Override
	public List<Vote> getVotesBySubject(Subject subject) throws SQLException {
		Session session = null;
		List<Vote> votes = new ArrayList<>();
		
		try {
			session = DataBaseConnectionUtil.getSessionFactory().openSession();
			List<Item>list = session.createQuery("from Item where subject="+subject.getId()).list();
			for(Item i:list)votes.addAll(i.getVotes());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return votes;
	}

}
