package by.votes.dao;

import java.sql.SQLException;
import java.util.List;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;

public interface IItemDAO {
	Item getItemById(long id) throws SQLException;
	List<Item>getItemsBySubject(Subject subject)throws SQLException;
	boolean addItem(Item item) throws SQLException;
	boolean updateItem(Item item) throws SQLException;
	boolean deleteItem(Item item) throws SQLException;
}
