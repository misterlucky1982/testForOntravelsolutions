package by.votes.service;

import java.util.List;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;

public interface IItemService {
	Item getItemById(long id);
	List<Item> getItemsBySubject(Subject subject);
	boolean addItem(Item item);
	boolean updateItem(Item item);
	boolean deleteItem(Item item);
}
