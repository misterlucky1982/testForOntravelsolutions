package by.votes.service;

import by.votes.entityes.Subject;
import by.votes.entityes.User;
import by.votes.entityes.Vote;

public interface IVoteService {
	Vote getVoteById(long id);
	boolean addVote(Vote vote);
	boolean updateVote(Vote vote);
	boolean deleteVote(Vote vote);
}
