package by.votes.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.votes.dao.IUserDAO;
import by.votes.entityes.User;

@Service
public class UserService implements IUserService{

	@Autowired
	private IUserDAO dao;
	
	
	@Override
	public User getById(long id) {
		// TODO Auto-generated method stub
		try{
			return this.dao.getById(id);
		}catch(SQLException e){
			return null;
		}
	}

	@Override
	public List<User> getUsersEntireList() {
		// TODO Auto-generated method stub
		try{
			return this.dao.getUsersEntireList();
		}catch(SQLException e){
			return new ArrayList<>();
		}
	}

	@Override
	public boolean addUser(User user) {
		// TODO Auto-generated method stub
		try{
			return this.dao.addUser(user);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		try{
			return this.dao.updateUser(user);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean deleteUser(User user) {
		// TODO Auto-generated method stub
		try{
			return this.dao.deleteUser(user);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public User getUserByLogin(String login) {
		try{
			return this.dao.getUserByLogin(login);
		}catch(SQLException e){
			return null;
		}
	}

}
