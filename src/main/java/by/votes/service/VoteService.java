package by.votes.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.votes.dao.IVoteDAO;
import by.votes.entityes.Vote;

@Service
public class VoteService implements IVoteService{

	@Autowired
	private IVoteDAO dao;
	
	@Override
	public Vote getVoteById(long id) {
		try{
			return this.dao.getVoteById(id);
		}catch(SQLException e){
			return null;
		}
	}

	@Override
	public boolean addVote(Vote vote) {
		try {
			return this.dao.addVote(vote);
		} catch (SQLException e) {
			return false;
		}
	}

	@Override
	public boolean updateVote(Vote vote) {
		try{
			return this.dao.updateVote(vote);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean deleteVote(Vote vote) {
		try{
			return this.dao.deleteVote(vote);
		}catch(SQLException e){
			return false;
		}
	}

	

	

}
