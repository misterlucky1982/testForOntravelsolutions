package by.votes.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.votes.dao.IItemDAO;
import by.votes.entityes.Item;
import by.votes.entityes.Subject;

@Service
public class ItemService implements IItemService{
	
	@Autowired
	private IItemDAO dao;

	@Override
	public Item getItemById(long id) {
		// TODO Auto-generated method stub
		try{
			return this.dao.getItemById(id);
		}catch(SQLException e){
			return null;
		}
	}

	@Override
	public boolean addItem(Item item) {
		// TODO Auto-generated method stub
		try{
			return this.dao.addItem(item);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean updateItem(Item item) {
		// TODO Auto-generated method stub
		try{
			return this.dao.updateItem(item);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean deleteItem(Item item) {
		// TODO Auto-generated method stub
		try{
			return this.dao.deleteItem(item);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public List<Item> getItemsBySubject(Subject subject) {
		try{
			return this.dao.getItemsBySubject(subject);
		}catch(SQLException e){
			return new ArrayList<Item>();
		}
	}
}
