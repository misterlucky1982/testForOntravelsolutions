package by.votes.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.votes.dao.ISubjectDAO;
import by.votes.entityes.Subject;
import by.votes.entityes.User;

@Service
public class SubjectService implements ISubjectService{

	@Autowired
	private ISubjectDAO dao;
	
	@Override
	public Subject getSubjectById(long id) {
		// TODO Auto-generated method stub
		try{
			return this.dao.getSubjectById(id);
		}catch(SQLException e){
			return null;
		}
	}

	@Override
	public List<Subject> getEntireSubjectsList() {
		try{
			return this.dao.getEntireSubjectsList();				
		}catch(SQLException e){
			return new ArrayList<Subject>();
		}
	}

	@Override
	public List<Subject> getOpenSubjectsList() {
		try{
			return this.dao.getOpenSubjectsList();
		}catch(SQLException e){
			return new ArrayList<Subject>();
		}
	}

	@Override
	public List<Subject> getClosedSubjectsList() {
		try{
			return this.dao.getClosedSubjectsList();
		}catch(SQLException e){
			return new ArrayList<Subject>();
		}
	}

	@Override
	public List<Subject> getSubjectsByUser(User user) {
		try{
			return this.dao.getSubjectsByUser(user);
		}catch(SQLException e){
			return new ArrayList<>();
		}
	}

	@Override
	public boolean addSubject(Subject subject) {
		try{
			return this.dao.addSubject(subject);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean ubdateSubject(Subject subject) {
		try{
			return this.dao.ubdateSubject(subject);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean deleteSubject(Subject subject) {
		try{
			return this.dao.deleteSubject(subject);
		}catch(SQLException e){
			return false;
		}
	}

	@Override
	public boolean isUserCanVote(User user, Subject subject) {
		return subject.isUserCanVote(user);//see remark for Subject.isUserCanVote
	}

	
	
	

}
