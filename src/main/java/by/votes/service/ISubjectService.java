package by.votes.service;

import java.util.List;

import by.votes.entityes.Subject;
import by.votes.entityes.User;

public interface ISubjectService {
	Subject getSubjectById(long id);
	List<Subject>getEntireSubjectsList();
	List<Subject>getOpenSubjectsList();
	List<Subject>getClosedSubjectsList();
	List<Subject>getSubjectsByUser(User user);
	boolean addSubject(Subject subject);
	boolean ubdateSubject(Subject subject);
	boolean deleteSubject(Subject subject);
	boolean isUserCanVote(User user, Subject subject);
}
