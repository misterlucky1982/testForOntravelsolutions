package by.votes.service;

import java.util.List;

import by.votes.entityes.User;

public interface IUserService {
	User getById(long id);
	List<User>getUsersEntireList();
	User getUserByLogin(String login);
	boolean addUser(User user);
	boolean updateUser(User user);
	boolean deleteUser(User user);	
}
