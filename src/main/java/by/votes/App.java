package by.votes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import by.votes.utils.DataBaseConnectionUtil;

@SpringBootApplication
public class App {

    public static void main(String[] args) {
    		SpringApplication.run(App.class, args);
    		DataBaseConnectionUtil.dataBaseInitialization();
    }
    
}
