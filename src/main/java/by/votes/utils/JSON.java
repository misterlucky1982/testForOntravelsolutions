package by.votes.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;
import by.votes.entityes.User;
import ch.qos.logback.classic.net.SyslogAppender;

@Component
public class JSON implements IJSONConverter{

	private JSONParser parser = new JSONParser();
	
	@Autowired
	private IPropertyes propertyes;
	
	@Override
	public User getCreatedUserFromJSON(String json) throws Exception {
		JSONArray array = (JSONArray)this.parser.parse(json);
		JSONObject ob = (JSONObject)array.get(0);
		String name = (String)ob.get("name");
		String login = (String)ob.get("login");
		String password = (String)ob.get("password");
		if(name==null||login==null||password==null)throw new Exception();
		return new User(name,login,password);
	}

	@Override
	public String longsToJSON(long... args) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String idInfoJSON(long id) {
		return "["+Long.toString(id)+"]";
	}

	@Override
	public Map<String, Object> getUserParametersFromJSON(String json) throws Exception {
		Map<String,Object>map = new HashMap<>();
		JSONArray array = (JSONArray)parser.parse(json);
		JSONObject ob = (JSONObject)array.get(0);
		map.put("login", (String)ob.get("login"));
		map.put("password", (String)ob.get("password"));
		Object id = null;
		try{
			id = (Long)ob.get("id");
		}catch(Exception e){}
		if(id!=null)map.put("id", id);
		return map;
	}

	@Override
	public List<String> getQuestionListFromJSON(String json) throws Exception {
		
		JSONArray array = (JSONArray)this.parser.parse(json);
		JSONObject ob = (JSONObject)array.get(0);
		
		array = (JSONArray)ob.get("items");
		System.out.println(array.toJSONString());
		Iterator it = array.iterator();
		List<String>list = new ArrayList<String>();
		while(it.hasNext())list.add((String)it.next());
		return list;
	}

	@Override
	public String getCurrentSubjectState(Subject subject) {
		if(subject==null)return "[]";
		JSONArray array = new JSONArray();
		JSONObject ob = new JSONObject();
		ob.put("subjectId", subject.getId());
		ob.put("question", subject.getMainQuestion());
		ob.put("status", subject.isStatus());
		JSONArray items = new JSONArray();
		Iterator it = subject.getItems().iterator();
		int count = 0;
		while(it.hasNext()){
			Item item = (Item) it.next();
			JSONObject ob2 = new JSONObject();
			ob2.put("item", item.getItemText());
			ob2.put("id", item.getId());
			ob2.put("ref", this.getItemReferenceForVote(item).toString());
			int votes = item.getVotes().size();
			count+=votes;
			ob2.put("votes", votes);
			items.add(ob2);
		}
		ob.put("allvotes", count);
		ob.put("items", items);
		array.add(ob);
		return array.toJSONString();
	}

	@Override
	public String userListToJsonInfo(List<User> list) {
		if(list==null)return "[]";
		JSONArray array = new JSONArray();
		for(User user:list)array.add(user.getName());
		return array.toJSONString();
	}
	
	@Override
	public String getSubjectReferenceInJSON(Subject subject){
		
		String ref = this.propertyes.getURLLine()+"/subjects/get?id="+subject.getId()+"&question="+subject.getMainQuestion();
		JSONArray array = new JSONArray();
		array.add(ref);
		return array.toJSONString();
	}
	
	private String getSubjectReference(Subject subject){
		
		return this.propertyes.getURLLine()+"/subjects/get?id="+subject.getId()+"&question="+subject.getMainQuestion();
		
	}

	private String getItemReferenceForVote(Item item) {
		return this.propertyes.getURLLine()+"/votes/vote?subject="+item.getSubject().getId()+"&item="+item.getId();
	}
	

	@Override
	public String subjectsListToJSON(List<Subject> list) {
		JSONArray array = new JSONArray();
		if(list == null)return "[]";
		for(Subject s:list){
			JSONObject ob = new JSONObject();
			ob.put("id", s.getId());
			ob.put("question", s.getMainQuestion());
			ob.put("status", s.isStatus());
			ob.put("ref", this.getSubjectReference(s));
			array.add(ob);
		}
		return array.toJSONString();
	}

	
}
