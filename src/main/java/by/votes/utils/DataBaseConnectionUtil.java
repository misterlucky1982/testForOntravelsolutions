package by.votes.utils;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

public class DataBaseConnectionUtil {

	private static final String DRIVER_CLASS = "org.h2.Driver";
	private static final String URL = "jdbc:h2:file:~/h2/votes;DB_CLOSE_ON_EXIT=TRUE";
	private static final String USERNAME = "sa";
	private static final String PASSWORD = "";
	private static final SessionFactory SESSION_FACTORY;
	private static final JdbcTemplate JDBC_TEMPLATE;
	private static final DataSource DATA_SOURCE;
	static {
		try {
			SESSION_FACTORY = getConfiguration().buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	static{
		DATA_SOURCE = new org.springframework.jdbc.datasource.DriverManagerDataSource(URL,USERNAME,PASSWORD);
	}
	static{
		JDBC_TEMPLATE = new JdbcTemplate(DATA_SOURCE);
	}

	public static SessionFactory getSessionFactory() {
		return SESSION_FACTORY;
	}

	@Bean
	public static Configuration getConfiguration() {
		Configuration configuration = new Configuration().setProperty("hibernate.connection.driver_class", DRIVER_CLASS)
				.setProperty("hibernate.connection.url", URL).setProperty("hibernate.connection.username", USERNAME)
				.setProperty("hibernate.connection.password", PASSWORD)
				.setProperty("hibernate.connection.autocommit", "false")
				.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider")
				.setProperty("hibernate.cache.use_second_level_cache", "false")
				.setProperty("hibernate.cache.use_query_cache", "false")
				.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
				.setProperty("hibernate.show_sql", "true")
				.setProperty("hibernate.current_session_context_class",
						"org.hibernate.context.internal.ThreadLocalSessionContext")
				.setProperty("hibernate.enable_lazy_load_no_trans", "true")
				.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "true")

				.addAnnotatedClass(by.votes.entityes.Vote.class).addAnnotatedClass(by.votes.entityes.Item.class)
				.addAnnotatedClass(by.votes.entityes.User.class).addAnnotatedClass(by.votes.entityes.Subject.class);

		return configuration;
	}
	
	@Bean
	public static JdbcTemplate getJdbcTemplate(){
		return JDBC_TEMPLATE;
	}
	
	@Bean 
	public static DataSource getDataSource(){
		return DATA_SOURCE;
	}
	
	public static void dataBaseInitialization(){
		String createUsers = "create table electors (id serial, name character varying (35) not null, login character varying (35) not null, password character varying (45) not null, primary key (id));";
		String createSubjects = "create table subjects (id serial, status boolean not null, creator bigint not null references electors(id), question character varying (255) not null, primary key (id));";
		String createItems = "create table items (id serial, subject bigint not null references subjects(id), text character varying (255), primary key(id));";
		String createVotes = "create table votes (id serial, item bigint not null references items(id), user bigint not null references electors(id), primary key (id)) ;";
		String createSequence = "CREATE SEQUENCE HIBERNATE_SEQUENCE START WITH 1 INCREMENT BY 1;";
		try{
		JDBC_TEMPLATE.execute(createUsers);
		System.out.println("users is created");
		}catch(Exception e){}
		try{
			JDBC_TEMPLATE.execute(createSubjects);
			System.out.println("subjects is created");
			}catch(Exception e){}
		try{
			JDBC_TEMPLATE.execute(createItems);
			System.out.println("items is created");
			}catch(Exception e){}
		try{
			JDBC_TEMPLATE.execute(createVotes);
			System.out.println("votes is created");
			}catch(Exception e){}
		try{
			JDBC_TEMPLATE.execute(createSequence);
			System.out.println("sequence is created");
			}catch(Exception e){}
	}

}
