package by.votes.utils;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;
import by.votes.entityes.User;

public interface IJSONConverter {

	User getCreatedUserFromJSON(String json) throws Exception;
	String longsToJSON(long...args);
	String idInfoJSON(long id);
	Map<String,Object>getUserParametersFromJSON(String json) throws Exception;
	List<String>getQuestionListFromJSON(String json) throws Exception;
	String getCurrentSubjectState(Subject subject);
	String userListToJsonInfo(List<User>list);
	String getSubjectReferenceInJSON(Subject subject);
	String subjectsListToJSON(List<Subject>list);
}
