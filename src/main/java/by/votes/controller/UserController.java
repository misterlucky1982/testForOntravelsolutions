package by.votes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import by.votes.entityes.User;
import by.votes.service.IUserService;
import by.votes.utils.IJSONConverter;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private IUserService userService;
	@Autowired
	private IJSONConverter jsonUtil;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@RequestBody (required=true) String json){
		User user = null;
		try {
			user = this.jsonUtil.getCreatedUserFromJSON(json);
			this.userService.addUser(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(user!=null&&user.getId()!=0){
			return this.jsonUtil.idInfoJSON(user.getId());
		}else return null;
	}
	
	@RequestMapping(value="/getusers", method = RequestMethod.GET)
	public String getUserList(){
		List<User>list = this.userService.getUsersEntireList();
		for(User user:list)System.out.println(user.toString());
		return this.jsonUtil.userListToJsonInfo(list);
	}
}
