package by.votes.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;
import by.votes.entityes.User;
import by.votes.service.IItemService;
import by.votes.service.ISubjectService;
import by.votes.service.IUserService;
import by.votes.utils.IJSONConverter;
import by.votes.utils.IPropertyes;

@RestController
@RequestMapping("/subjects")
public class SubjectController {
	
	@Autowired
	private ISubjectService subjectService;
	@Autowired
	private IUserService userSevice;
	@Autowired
	private IItemService itemService;
	@Autowired
	private IJSONConverter json;
	@Autowired
	private IPropertyes propertyes;
	
	@RequestMapping(value = "/newsubject", method = RequestMethod.POST)
	public String register(@RequestParam(value = "user", required = true) long userId,
			@RequestParam(value = "question",required = true) String question,
			@RequestBody (required=true) String body){
		User user = this.userSevice.getById(userId);
		if(user==null)return null;
		Map<String,Object>parameters = null;
		try{
			parameters = this.json.getUserParametersFromJSON(body);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		if(!user.getLogin().equals(parameters.get("login")))return null;
		if(!user.getPassword().equals(parameters.get("password")))return null;
		Subject subject = new Subject(user,question);
		this.subjectService.addSubject(subject);
		List<String>questions = null;
		try{
			questions = this.json.getQuestionListFromJSON(body);
		}catch(Exception e){
			e.printStackTrace();
		}
		for(String q:questions){
			Item item = new Item(subject,q);
			this.itemService.addItem(item);
		}
		return this.json.getSubjectReferenceInJSON(subject);
		/*
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append('"');
		sb.append(this.propertyes.getURLLine()+"/subjects/get?id="+subject.getId()+"&question="+subject.getMainQuestion());
		sb.append('"');
		sb.append("]");
		return sb.toString();*/
	}
	
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public String getCurrentSubjectState(@RequestParam(value = "id",required = true) long id,
			@RequestParam(value = "question",required = true) String question){
		Subject subject = this.subjectService.getSubjectById(id);
		if(subject.getMainQuestion().equals(question)){
			return this.json.getCurrentSubjectState(subject);
		}else return null;
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String getSubjectsList(@RequestParam(value = "status", required = false) Boolean status){
		if(status==null)return this.json.subjectsListToJSON(this.subjectService.getEntireSubjectsList());
		if(status==true){
			return this.json.subjectsListToJSON(this.subjectService.getOpenSubjectsList());
		}else return this.json.subjectsListToJSON(this.subjectService.getClosedSubjectsList());
	}
	
	@RequestMapping(value = "close", method = RequestMethod.PUT)
	public String finish(@RequestParam(value = "id", required = true) long id,
			@RequestBody (required = true) String body){
		Subject subject = this.subjectService.getSubjectById(id);
		if(subject==null)return null;
		User creator = subject.getCreator();
		Map<String,Object>map = null;
		try{
			map = this.json.getUserParametersFromJSON(body);
		}catch(Exception e){
			return null;
		}
		Long userId = (Long)map.get("id");
		if(userId==null)return null;
		if(creator.getId()!=userId)return null;
		if(creator.getLogin().equals(map.get("login"))){
			if(creator.getPassword().equals(map.get("password"))){
				subject.setStatus(false);
				this.subjectService.ubdateSubject(subject);
				return this.json.getCurrentSubjectState(subject);
			}
		}
		return null;
	}
}
