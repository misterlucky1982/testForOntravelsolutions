package by.votes.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import by.votes.entityes.Item;
import by.votes.entityes.Subject;
import by.votes.entityes.User;
import by.votes.entityes.Vote;
import by.votes.service.IItemService;
import by.votes.service.ISubjectService;
import by.votes.service.IUserService;
import by.votes.service.IVoteService;
import by.votes.utils.IJSONConverter;

@RestController
@RequestMapping("/votes")
public class VoteController {
	
	@Autowired
	private IVoteService voteService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ISubjectService subjectService;
	@Autowired
	private IItemService itemService;
	@Autowired
	private IJSONConverter jsonUtil;
	
	//vote?subject=...&item=... + body
	@RequestMapping(value="/vote")
	private String vote(@RequestParam(value = "subject",required = true)long subjectId,
			@RequestParam(value = "item", required = true)long itemId,
			@RequestBody(required=true) String body){
		Map<String,Object>map = null;
		try{
			map = this.jsonUtil.getUserParametersFromJSON(body);
		}catch(Exception e){
			return null;
		}
		Object id = map.get("id");
		User user = null;
		if(id instanceof Long)user = this.userService.getById((Long)id);
		if(user==null)return null;
		if(!user.getLogin().equals(map.get("login")))return null;
		if(!user.getPassword().equals(map.get("password")))return null;
		Subject subject = this.subjectService.getSubjectById(subjectId);
		if(subject==null)return null;
		if(!subject.isStatus())return null;
		if(!this.subjectService.isUserCanVote(user, subject))return null;
		Item item = this.itemService.getItemById(itemId);
		if(item==null)return null;
		Vote vote = new Vote(user,item);
		this.voteService.addVote(vote);
		return this.jsonUtil.idInfoJSON(vote.getId());		
	}
	

}
